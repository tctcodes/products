import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNewProduct } from './actions/productActions';

class NewProductForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      category: '',
      price: '',
      name: '',
      stock: true,
    };
    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const product = {
      category: this.state.category,
      price: this.state.price,
      name: this.state.name,
      stocked: this.state.stocked,
    }
    this.props.addNewProduct(product);
  }

  render() {
    return (
      <div>
        <form id="new-product-form" onSubmit={this.handleSubmit} >
          <label>
            Category:
            <input 
              type="text"
              name="category"
              onChange={this.onChange}
              value={this.state.category}
            />
          </label>
          <br/>
          <label>
            Name:
            <input 
              type="text"
              name="name"
              onChange={this.onChange}
              value={this.state.name}

            />
          </label>
          <br />
          <label>
            Price:
            <input
              type="text"
              name="price"
              onChange={this.onChange}
              value={this.state.price}
            />
          </label>
          <br />
          <label>
            In Stock:
            <input
              type="checkbox"
              name="stocked"
              onChange={this.onChange}
              value={this.state.stocked}
            />
          </label>
          <br />
          <br />
          <input type="submit" />
        </form>
      </div>
    );
  }
}

NewProductForm.propTypes = {
  addNewProduct: PropTypes.func.isRequired,
}

export default connect(null, { addNewProduct })(NewProductForm);