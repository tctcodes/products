import React, { Component } from 'react';
import SearchBar from './SearchBar';
import ProductTable from './ProductTable';
import NewProductForm from './NewProductForm';

class FilterableProductTable extends Component {
  render() {
    return (
      <div>
        <NewProductForm/>
        <SearchBar/>
        <ProductTable/>
      </div>
    );
  }
}

export default FilterableProductTable;
