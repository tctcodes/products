import { HANDLE_FILTER_TEXT, HANDLE_STOCK_STATUS, ADD_NEW_PRODUCT, UPDATE_STOCK } from './types';

export function handleFilterText(text) {
    return function(dispatch) {
        dispatch({
            type: HANDLE_FILTER_TEXT,
            payload: text,
        });
    };
};

export function handleStockStatus(inStockOnly) {
    return function(dispatch) {
        dispatch({
            type: HANDLE_STOCK_STATUS,
            payload: inStockOnly,
        });
    };
};

export function addNewProduct(product) {
    return function(dispatch) {
        dispatch({
            type: ADD_NEW_PRODUCT,
            payload: product,
        });
    };
};

export function updateStock(name) {
    return function(dispatch) {
        dispatch({
            type: UPDATE_STOCK,
            payload:name,
        });
    };
};

