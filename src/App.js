import React, { Component } from 'react';
import { Provider } from 'react-redux';
import './App.css';
import store from './store';
import FilterableProductTable from './FilterableProductTable';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <FilterableProductTable />
        </div>
      </Provider>
    );
  }
}

export default App;
