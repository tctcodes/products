import React, { Component } from 'react';
import { connect } from 'react-redux';
import { handleFilterText } from './actions/productActions';
import { handleStockStatus } from './actions/productActions';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleInStockChange = this.handleInStockChange.bind(this);
  }
  
  handleFilterTextChange(e) {
    this.props.handleFilterText(e.target.value);
  }
  
  handleInStockChange(e) {
    this.props.handleStockStatus(e.target.checked);
  }
  
  render() {
    return (
      <form>
        <input
          type="text"
          placeholder="Search..."
          onChange={this.handleFilterTextChange}
        />
        <p>
          <input
            type="checkbox"
            checked={this.props.inStockOnly}
            onChange={this.handleInStockChange}
          />
          {' '}
          Only show products in stock
        </p>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  filterText: state.product.filterText,
  inStockOnly: state.product.inStockOnly,
});

const mapDispatchToProps = {
  handleFilterText: handleFilterText,
  handleStockStatus: handleStockStatus,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
